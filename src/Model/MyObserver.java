package ChatWithSockets.src.Model;

import java.util.Observable;

public interface MyObserver {
	
	public abstract void update();
	
	/**
	 * This method is called when the Observable want to update their Observers
	 * when their state/condition has changed.
	 * 
	 * @param obs
	 *            - the Observable that has changed
	 * @param optionalArg
	 *            - any optional argument that the Observable might send. If
	 *            there is no optional argument sent, then this will be null.
	 */
	public abstract void update(Observable obs, Object optional);
}