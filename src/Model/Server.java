package ChatWithSockets.src.Model;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

/**
 * Server class that waits for incoming connections, each new connection is
 * added to a new thread that will handle only that client.
 * 
 * @author Michael Humphrey
 * 
 */
public class Server implements Runnable {

	private ServerSocket myServerSocket;
	private Vector<Liason> connectClients; // Vector used because arrayList is not thread safe
	private Vector<String> messages;
	private final static int PORT_NUMBER = 4001;

	public static void main(String args[]) {
		Server server = new Server();
		Thread serverThread = new Thread(server);
		serverThread.start();
	}

	public Server() {
		connectClients = new Vector<Liason>();
		messages = new Vector<String>();
		try {
			myServerSocket = new ServerSocket(PORT_NUMBER);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (true) {
				// accept blocks until request comes over socket
				Socket intoServer = myServerSocket.accept();
				System.out.println("Is waiting for connect" + intoServer);
				// For every new connection, start a new Thread that will
				// communicate with that client. Each one will have access
				// to the common collection of all users who are connected.
				Liason aThreadForOneClient = new Liason(intoServer,
						connectClients, messages);
				Thread thread = new Thread(aThreadForOneClient);
				connectClients.add(aThreadForOneClient);
				// always call the start() method on a Thread.
				thread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
