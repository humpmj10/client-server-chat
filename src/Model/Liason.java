package ChatWithSockets.src.Model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

/**
 * Each instance of this class allows a client to interact with the database.
 * Currently the functionality is to allow a client to be added to the
 * collection of concurrently connected clients, to quit which removes them from
 * the database, and to also get a list of all people's names who are currently
 * connected. The client can enter these three requests for services:
 * 
 * @author Michael Humphrey
 * 
 */

public class Liason implements Runnable {

	private Vector<Liason> sharedCollectionReference; // record of the connect clients to the server
	private Vector<String> messages; // record of the messages being broadcast
	private Socket socketFromServer; // socket that connects to server
	private ObjectInputStream readFromClient; // object input stream that reads from the client
	private ObjectOutputStream writeToClient; // object output stream that will write out to client

	/**
	 * Construct an object that will run in it's own Thread so this object can
	 * communicate with that one connection. When this client quits, all messages
	 * that client entered are removed from the sharedCollectionReference.
	 */
	public Liason(Socket socketFromServer,
			Vector<Liason> sharedCollectionReference,
			Vector<String> messages) {
		this.socketFromServer = socketFromServer;
		this.sharedCollectionReference = sharedCollectionReference;
		this.messages = messages;
	}

	@Override
	public void run() {
		// Open the input and output streams so the
		// client can interact with the collection
		try {
			readFromClient = new ObjectInputStream(
					socketFromServer.getInputStream());
			writeToClient = new ObjectOutputStream(
					socketFromServer.getOutputStream());
		} catch (IOException e) {
			System.out
					.println("Exception thrown while obtaining input & output streams");
			e.printStackTrace();
			return;
		}

		String messageFromClient = null;
		boolean wantToAStayConnected = true;

		// Loop as long as we are connected using a boolean variable
		while (wantToAStayConnected) {
			try {
				messageFromClient = (String) readFromClient.readObject();
			} catch (IOException e1) {
				System.out
						.println("IOException in ServerThread.run when reading from client");
				return;
			} catch (ClassNotFoundException e1) {
				System.out
						.println("ClassCastException in ServerThread.run casting object from client to String");
				e1.printStackTrace();
			}

			// Now that we have read a valid string from Client, process it:
			try { // Process a "put Name"
				if (messageFromClient.length() >= 3
						&& messageFromClient.substring(0, 3).equalsIgnoreCase(
								"put")) {
					String clientMessage = messageFromClient.substring(3)
							.trim();
					messages.add(clientMessage);
					for (int i = 0; i < sharedCollectionReference.size(); i++) {
						Liason temp = sharedCollectionReference
								.get(i);
						temp.writeToClient.reset();
						temp.writeToClient.writeObject(clientMessage);
						temp.writeToClient.flush();
					}

				}

				if (messageFromClient.equalsIgnoreCase("get")) { // Process a "get"
					writeToClient.reset();
					writeToClient.writeObject(messages);
				}

				if (messageFromClient.equalsIgnoreCase("quit")) { // Process a "quit"
					// Clean up and avoid exceptions
					sharedCollectionReference.remove(this);
					socketFromServer.close();
					readFromClient.close();
					writeToClient.close();
					wantToAStayConnected = false;
				}

			} catch (Exception e) {
				e.printStackTrace();
				wantToAStayConnected = false;
			}
		}
	}

}
