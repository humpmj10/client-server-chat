package ChatWithSockets.src.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * GUI class for our client object. Takes input from the user and broadcasts that to the 
 * server. Currently the client is set to connect to local host only, but can be easily modified
 * to connect to host over the internet or networked computers.
 * 
 * @author Michael Humphrey
 *
 */
public class ClientGUI extends JFrame {

	public static final String HOST_NAME = "localhost";

	private JTextField chatInput; // text area for the chat input
	private JTextArea chatText; // 
	private JLabel chatLabel; // label to indicate the chat field
	private JPanel main; // main panel for gui
	private Thread startReader;
	private boolean wantToStayConnected;

	public static final int PORT_NUMBER = 4001;
	public String myName;

	private ObjectOutputStream outputToLiasonLoop; // stream to server
	private ObjectInputStream inputFromLiasonLoop; // stream from server

	public static void main(String args[]) {
		ClientGUI client = new ClientGUI();
		client.setVisible(true);
		client.setSize(450, 450);
		client.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client.startReader();
	}

	public ClientGUI() {

		main = new JPanel();

		main.add(chatLabel = new JLabel("Chat Input"));
		main.add(chatInput = new JTextField(20));
		this.add(main, BorderLayout.NORTH);
		this.add(chatText = new JTextArea(), BorderLayout.CENTER);
		chatText.setEditable(false);
		chatText.setLineWrap(true);
		chatText.setForeground(Color.BLUE);
		chatText.setFont(new Font("Calibri", 0, 14));

		while (getName() == null) {
			getName();

		}

		registerListeners();

	}

	public void startReader() {
		wantToStayConnected = true;
		connectToServer();
		IncomingReader reader = new IncomingReader();
		startReader = new Thread(reader);
		startReader.start();

	}

	/**
	 * Method called to get the name for the client, will be displayed in the GUI, so that 
	 * each party will know who typed which message.
	 */
	public String getName() {
		String s = (String) JOptionPane.showInputDialog(null,
				"Name needed for chat\n" + "\"Please enter your name.\"",
				"Name Chooser", JOptionPane.PLAIN_MESSAGE, null, null, null);
		myName = s;
		return s;
	}

	/**
	 * Register the listeners for the close button and chat input listener.
	 */
	public void registerListeners() {

		CloseListener close = new CloseListener();
		this.addWindowListener(close);

		ChatListener chatListener = new ChatListener();
		chatInput.addActionListener(chatListener);
	}

	/**
	 * Called to connect the client to the server. Will display error message if unable to connect or if
	 * any other problems arise.
	 */
	public void connectToServer() {
		Socket sock = null;
		try {
			// connect to the server
			sock = new Socket(HOST_NAME, PORT_NUMBER);
		} catch (Exception e) {
			System.out.println("Client was unable to connect to server");
			e.printStackTrace();
		}
		try {
			outputToLiasonLoop = new ObjectOutputStream(sock.getOutputStream());
			inputFromLiasonLoop = new ObjectInputStream(sock.getInputStream());
			outputToLiasonLoop.writeObject("put " + myName
					+ " has entered the chatroom.");
		} catch (Exception e) {
			System.out
					.println("Unable to obtain Input/Output streams from Socket");
			e.printStackTrace();
		}
	}

	/**
	 * Listens to the text field where chat is inputed. Sends a message to the server once
	 * enter is pressed.
	 */

	private class ChatListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {

			try {
				if (chatInput.getText().equalsIgnoreCase("quit")) {
					wantToStayConnected = false;
					outputToLiasonLoop.writeObject("quit");
					System.exit(0);
				} else {
					outputToLiasonLoop.writeObject("put " + myName + ": "
							+ chatInput.getText());
					chatInput.setText("");
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * Needed so that the GUI doesn't freeze up when waiting for connections
	 * It's a private class that starts in a new the thread.
	 * 
	 * @author Michael Humphrey
	 * 
	 */
	private class IncomingReader implements Runnable {

		public void run() {

			while (wantToStayConnected) {
				String message = null;
				try {

					message = (String) inputFromLiasonLoop.readObject();
				} catch (Exception e) {
					break;
				}
				chatText.append("\n" + message);

			}
		}
	}

	/**
	 * Used to listen to window closing, makes sure the connections are closed when the window is closed.
	 *
	 * @author Michael Humphrey and Brian Smith
	 *
	 */
	private class CloseListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosed(WindowEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosing(WindowEvent arg0) {
			wantToStayConnected = false;
			try {
				outputToLiasonLoop.writeObject("quit");
				outputToLiasonLoop.close();
				inputFromLiasonLoop.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);

		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

}
